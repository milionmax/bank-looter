# Bank Looter

This is the start of my attempt to recreate the functionality of the old Bank Robber software intended to make loading and managing samples on the Roland SP-404SX/A easier and more powerful than with the Roland-provided software. Additionally, this is built as an Electron app so should run on most platforms (Win, macOS, Linux).

*Note: this project is not under current development and I have no plans to come back to it in the future. That being said, you may use the code as you see fit. I make no promises as to the current functionality, lack of bugs, etc.*

## HOW TO RUN

1. Clone this and the [SP-404 Tools](https://gitlab.com/cflann/sp404-tools) repository.
2. Run `yarn install`, `yarn build`, and `yarn link` in SP-404 Tools root directory.
3. Run `yarn install`, `yarn link sp404-tools`, and `yarn start` in this repo.
4. Troubleshoot as necessary.
