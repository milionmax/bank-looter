import React from 'react'
import { useSelector } from 'react-redux'

import AppState from '../store'
import { Container } from './style'
import BlockingModal from './BlockingModal'
import EllipsisMessage from './EllipsisMessage'
import DriveManager from '../containers/DriveManager'
import BankManager from '../containers/BankManager'

const App: React.FC = (props) => {
  const { exporting } = useSelector(
    (state: AppState) => state.app
  )

  return (
    <Container>
      {exporting && (
        <BlockingModal>
          <EllipsisMessage text='Exporting' />
        </BlockingModal>
      )}
      <DriveManager />
      <BankManager />
    </Container>
  )
}

export default App
