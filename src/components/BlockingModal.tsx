import React from 'react'
import styled from 'styled-components'

const Background = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  z-index: 1;
  width: 100%;
  height: 100%;
  background-color: rgba(50, 50, 50, 0.5);
  text-align: center;
`

const Body = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  width: 100%;
  height: 50%;
  position: relative;
  top: 25%;
  background-color: white;
`

const BlockingModal: React.FC = (props) => {
  return (
    <Background>
      <Body>
        {props.children}
      </Body>
    </Background>
  )
}

export default BlockingModal
