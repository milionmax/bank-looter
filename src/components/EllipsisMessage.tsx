import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  font-size: 20px;  
`

const EllipsisWrap = styled.span`
  width: 20px;
  display: inline-block;
  text-align: left;
`

class EllipsisMessage extends React.Component<{ text: string }, { frames: number }> {
  state = {
    frames: 0
  }

  componentDidMount() {
    setInterval(() => {
      this.setState({ frames: this.state.frames + 1 })
    }, 500)  
  }

  render() {
    const x = this.state.frames % 3 + 1
    const ellipsis = ''.padEnd(x, '.')
    return <Wrapper>{this.props.text}<EllipsisWrap>{ellipsis}</EllipsisWrap></Wrapper>
  } 
}

export default EllipsisMessage
