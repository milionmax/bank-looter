import React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'

import Button from '../components/Button'

export default {
  title: 'BankLooter/Button',
  component: Button,
} as Meta

export const Default = (args) => <Button>Default</Button>
export const Primary = (args) => <Button primary>Primary</Button>
export const Warning = (args) => <Button warning>Warning</Button>
export const Danger = (args) => <Button danger>Danger</Button>
export const Disabled = (args) => <Button disabled>Disabled</Button>
