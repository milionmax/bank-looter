import { ipcRenderer } from 'electron'
import { Dispatch } from 'redux'
import { createSlice } from '@reduxjs/toolkit'

import { Drive, Sample, SPImporter, SPExporter } from 'sp404-tools'

import { AppThunk } from '../store'
import { exportStart, exportSuccess, exportFailure } from './appSlice'
import { loadImport } from './samplesSlice'

const driveSlice = createSlice({
  name: 'drive',
  initialState: {
    path: ''
  },
  reducers: {
    setDrivePath(state, action) {
      state.path = action.payload.path
    }
  }
})

export const { setDrivePath } = driveSlice.actions

export default driveSlice.reducer

export const selectDrive = (): AppThunk => (async (dispatch: Dispatch) => {
  const { canceled, filePaths } = await ipcRenderer.invoke('select-drive')
  if (!canceled) {
    dispatch(setDrivePath({ path: filePaths.pop() }))
  }
})

export const importDrive = (): AppThunk => (async(dispatch: Dispatch, getState) => {
  const importer = new SPImporter({ path: getState().drive.path })
  const samples: Sample[] = await importer.importSamples()
  dispatch(loadImport({ samples }))
})

export const exportDrive = (): AppThunk => (async(dispatch: Dispatch, getState) => {
  dispatch(exportStart())
  const { drive: { path }, samples } = getState()
  const exporter = new SPExporter({ path })
  try {
    const exported = await exporter.exportSamples(samples)
    await exporter.exportPadInfoBin(samples)
    dispatch(loadImport({ samples: exported }))
    dispatch(exportSuccess())
  } catch(error) {
    dispatch(exportFailure({ error }))
  }
})
